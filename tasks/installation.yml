---

- name: git clone repository - use version {{ graphyte_daemon_git_version }}
  git:
    repo: "{{ graphyte_daemon_git_url }}"
    version: "{{ graphyte_daemon_git_version }}"
    dest: "{{ graphyte_daemon_git_clone_dest }}/{{ graphyte_daemon_git_version }}"
    update: "{{ graphyte_daemon_git_update }}"
    archive: "{{ graphyte_daemon_git_clone_dest }}/{{ graphyte_daemon_archive }}"
  delegate_to: localhost
  become: false

- name: create tempoary directory
  file:
    path: /tmp/graphyte-daemon
    state: directory
    mode: 0755

- name: propagate {{ graphyte_daemon_archive }}
  become: true
  copy:
    src: "/tmp/graphyte-daemon/{{ graphyte_daemon_archive }}"
    dest: "/tmp"
    mode: 0750

- name: extract {{ graphyte_daemon_archive }}
  unarchive:
    src: "/tmp/{{ graphyte_daemon_archive }}"
    dest: /tmp/graphyte-daemon
    remote_src: true

- name: install python3 requirements
  pip:
    executable: pip3
    # extra_args: --upgrade
    requirements: /tmp/graphyte-daemon/requirements.txt
  register: pip_install
  ignore_errors: true
  no_log: true

- block:
    - name: install build essentials
      package:
        name: "{{ build_packages }}"
        state: present

    - name: install python3 requirements 2nd
      pip:
        executable: pip3
        # extra_args: --upgrade
        requirements: /tmp/graphyte-daemon/requirements.txt

    - name: uninstall build essentials
      package:
        name: "{{ build_packages }}"
        state: absent

  when: pip_install.failed

- name: install python3 requirements for mysql
  pip:
    executable: pip3
    name: mysqlclient
  when:
    graphyte_services.services.mysql is defined

- name: install python3 requirements for mongodb
  pip:
    executable: pip3
    name: pymongo
  when:
    graphyte_services.services.mongodb is defined

- name: install files into {{ graphyte_daemon_user }} user home  # noqa 301 303
  shell: |
    /usr/bin/rsync \
      --delay-updates \
      --archive \
      --compress \
      --recursive \
      --delete \
      --exclude=.git \
      --verbose \
      --blocking-io \
      --exclude *.txt \
      --exclude *.md \
      --exclude graphyte-daemon.yml.example \
      --chown {{ graphyte_daemon_user }}:{{ graphyte_daemon_group }} \
      /tmp/graphyte-daemon/* \
      {{ graphyte_daemon_home }}/bin/
  delegate_to: "{{ inventory_hostname }}"

- name: ensure binary are executable
  file:
    path: "{{ graphyte_daemon_home }}/bin/graphyte-daemon.py"
    mode: 0750

- name: create systemd service unit
  template:
    src: init/systemd/graphyte-daemon.service.j2
    dest: "{{ systemd_lib_directory }}/graphyte-daemon.service"
    owner: root
    group: root
    mode: 0644
