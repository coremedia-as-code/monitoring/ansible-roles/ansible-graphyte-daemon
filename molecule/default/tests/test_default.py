import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/home/graphyte-daemon",
    "/home/graphyte-daemon/bin/",
    "/home/graphyte-daemon/bin/plugins"
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/home/graphyte-daemon/bin/graphyte-daemon.py",
    "/home/graphyte-daemon/graphyte-daemon.yml"
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


@pytest.mark.parametrize("pip", [
    "requests"
])
def test_pip_packages(host, pip):
    p = host.pip_package.get_packages(pip_path="/usr/bin/pip3")
    print(p)
